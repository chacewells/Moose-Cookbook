use feature 'say';
package Point;
use Moose;

has x => (isa => 'Int', is => 'rw', required => 1);
has y => (isa => 'Int', is => 'rw', required => 1);

sub clear {
    my $self = shift;
    $self->x(0);
    $self->y(0);
}

package Point3D;
use Moose;

extends 'Point';

has z => (isa => 'Int', is => 'rw', required => 1);

after clear => sub {
    my $self = shift;
    $self->z(0);
};

package main;

my$point1 = Point->new(x => 5, y => 7);
my$point2 = Point->new({x => 5, y => 7});

my$point3d = Point3D->new(x => 5, y => 42, z => -5);

do {
    say 'x: ', $_->x, ' y: ', $_->y, ($_->can('z') ? '; z: '.$_->z : '');
} for $point1, $point2, $point3d;

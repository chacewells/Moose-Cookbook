use feature 'say';
package Point;

sub new {
    my( $class, %self ) = @_;
    _validate($self{x} //= 0);
    _validate($self{y} //= 0);
    bless \%self, $class;
}

sub x {
    my $self = shift;
    if (defined(my $x = shift)) {
        _validate($x) or die $!;
        $self->{x} = $x;
        return 1;
    }
    $self->{x}
}

sub y {
    my $self = shift;
    if (defined(my $y = shift)) {
        _validate($y) or die $!;
        $self->{y} = $y;
        return 1;
    }
    $self->{y}
}

sub _validate {
    my $n = shift;
    $n =~ /^(\d*\.\d+|\d+)$/ or die "Invalid coordinate: $n";
    $n;
}

sub clear {
    my $self = shift;
    $self->x(0);
    $self->y(0);
}

package BinaryTree;
use Moose;

has node => ( is => 'rw', isa => 'Any' );

has parent => (
    is        => 'rw',
    isa       => 'BinaryTree',
    predicate => 'has_parent',
    weak_ref  => 1,
);

has left => (
    is          => 'rw',
    isa         => 'BinaryTree',
    predicate   => 'has_left',
    lazy        => 1,
    default     => sub { BinaryTree->new( parent => $_[0] ) },
    trigger     => \&_set_parent_for_child
);

has right => (
    is          => 'rw',
    isa         => 'BinaryTree',
    predicate   => 'has_right',
    lazy        => 1,
    default     => sub { BinaryTree->new( parent => $_[0] ) },
    trigger     => \&_set_parent_for_child
);

sub insert {
    my ( $self, $val ) = @_;
    $self->_insert_here( $val, 'left' ) if $val < $self->node;
    $self->_insert_here( $val, 'right' ) if $val > $self->node;
}


sub _insert_here {
    my ( $self, $val, $branch ) = @_;
    my $has_branch = 'has_'.$branch;
    $self->$has_branch
         ? $self->$branch->insert( $val )
         : $self->$branch( BinaryTree->new( node => $val ) );
}

sub _set_parent_for_child {
    my ( $self, $child ) = @_;

    confess "You cannot insert a tree which already has a parent"
        if $child->has_parent;

    $child->parent($self);
}

no Moose;

package main;
use feature 'say';

my $parent = BinaryTree->new( node => 50 );
for (1..100) {
    $parent->insert($_)
}

traverse( $parent );

sub traverse {
    my $node = shift;
    traverse( $node->left ) if $node->has_left;
    say $node->node;
    traverse( $node->right ) if $node->has_right;
}

my $lonely = BinaryTree->new(node => 5);
say( $lonely->has_left ? "has left" : "no left" );
say( $lonely->has_right ? "has right" : "no right" );

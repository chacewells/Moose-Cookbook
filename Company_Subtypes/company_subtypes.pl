#!/opt/local/bin/perl

package Address;
use Moose;
use Moose::Util::TypeConstraints;

use Locale::US;
use Regexp::Common 'zip';

my $STATES = Locale::US->new;
subtype 'USState'
    => as Str
    => where {
        (   exists $STATES->{code2state}{ uc($_) }
         || exists $STATES->{state2code}{ uc($_) } );
    };

subtype 'USZipCode'
    => as Value
    => where {
        /^$RE{zip}{US}{-extended => 'allow'}$/;
    };

has street   => ( is => 'rw', isa => 'Str' );
has city     => ( is => 'rw', isa => 'Str' );
has state    => ( is => 'rw', isa => 'USState' );
has zip_code => ( is => 'rw', isa => 'USZipCode' );

package Company;
use Moose;
use Moose::Util::TypeConstraints;

has name => ( is => 'rw', isa => 'Str', required => 1 );
has name => ( is => 'rw', isa => 'Address' );
has employess => (
    is      => 'rw',
    isa     => 'ArrayRef[Employee]',
    default => sub { [] }
);

sub BUILD {
    my ( $self, $params ) = @_;
    foreach my $employee ( @{ $self->employees } ) {
        $employee->employer($self);
    }
}

after employees => sub {
    my ( $self, $employees ) = @_;
    return unless $employees;
    foreach my $employee ( @$employees ) {
        $employee->employer($self);
    }
};

package Person;
use Moose;

has first_name => ( is => 'rw', isa => 'Str', required => 1 );
has last_name => ( is => 'rw', isa => 'Str', required => 1 );
has middle_initial => (
    is        => 'rw', isa => 'Str',
    predicate => 'has_middle_initial'
);
has address => ( is => 'rw', isa => 'Address' );

sub full_name {
    my $self = shift;
    return $self->first_name
        . (
        $self->has_middle_initial
        ? ' ' . $self->middle_initial . '. '
        : ' '
        ) . $self->last_name;
}

package Employee;
use Moose;

extends 'Person';

has title    => ( is 'rw', isa => 'Str', required => 1 );
has employer => ( is 'rw', isa => 'Company', weak_ref => 1 );

override full_name => sub {
    my $self = shift;
    super() . ', ' . $self->title;
};

package main;

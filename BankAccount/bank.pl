use feature 'say';

package BankAccount;
use Moose;

has balance => ( isa => 'Int', is => 'rw', default => 0 );

sub deposit {
    my( $self, $amount ) = @_;
    $self->balance( $self->balance + $amount );
}

sub withdraw {
    my( $self, $amount ) = @_;
    my $current_balance = $self->balance;
    ( $current_balance >= $amount )
        || confess "Account overdrawn";
    $self->balance( $current_balance - $amount );
}

package CheckingAccount;
use Moose;

extends 'BankAccount';

has overdraft_account => ( isa => 'BankAccount', is => 'rw' );

before withdraw => sub {
    my( $self, $amount ) = @_;
    my $overdraft_amount = $amount - $self->balance;
    if ( $self->overdraft_account && $overdraft_amount > 0 ) {
        $self->overdraft_account->withdraw($overdraft_amount);
        $self->deposit($overdraft_amount);
    }
};

no Moose;

package main;

my $savings = BankAccount->new(balance => 300);
my $checking = CheckingAccount->new(balance => 140, overdraft_account => $savings);

sub show_balances {
    say 'savings: ', $savings->balance;
    say 'checking: ', $checking->balance;
}

show_balances();
say 'withdrawing $200';
$checking->withdraw(200);
show_balances();
